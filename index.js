var axios = require('axios');
const cron = require('node-cron');
const redis = require('redis')
var express = require('express');

const client = redis.createClient(6379);

var app = express();

/**
 * Get Tehran Weather api
 */
app.get('/get-tehran-weather', function (req, res) {
    // Get weather data from Redis
    return client.get('tehran-weather', (err, data) => {
        // If tehran-weather key exists in Redis store
        if (data) {
            return res.status(200).send(JSON.parse(data));
        } else {
            return res.status(404).send('Tehran weather data not found!');
        }
    });
});

cron.schedule('*/5 * * * *', function () {
    console.log('cron started');

    let api_token = 'f3f30b1dde9650d97b3f85bccaa47fd4'; //Open Weather Map api token
    let city = 'Tehran'

    // Use axios for get forcast of tehran
    axios.get(`http://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${api_token}`)
        .then(weather_data => {
            // Use Redis for store weather data
            client.setex('tehran-weather', 1800, JSON.stringify(weather_data.data)); // store weather data for 30 minutes
        });
});

var server = app.listen(process.env.PORT || 5000, function () {
    var port = server.address().port;
    console.log("Weather app is working on port " + port);
  });
